/**
 * $File: main.cpp $
 * $Date: 2023-04-01 19:31:33 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2023 by Shen, Jen-Chieh $
 */
#include <iostream>
#include <string>

int main(int argc, char **argv) {
    int count = 1;
    std::string name;
    while (count < argc) {
        name += argv[count++];
    }
    name.erase(0, name.find_last_not_of(" \t\n\r\f\v"));
    name.erase(name.find_last_not_of(" \t\n\r\f\v") + 1);
    std::cout << name << std::endl;
    return 0;
}
